package com.hikkidev.ext.string

import android.text.TextUtils
import java.security.MessageDigest

// RFC 5322 Official Standard Email Pattern https://www.ietf.org/rfc/rfc5322.txt
private const val emailPattern =
    "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*" +
            "|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\" +
            "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+" +
            "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.)" +
            "{3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\" +
            "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"

private const val phonePattern =
    "(\\+[0-9]+[\\- \\.]*)?" + "(\\([0-9]+\\)[\\- \\.]*)?" + "([0-9][0-9\\- \\.]+[0-9])"

private val emailRegex by lazy { Regex(emailPattern) }
private val phoneRegex by lazy { Regex(phonePattern) }

/**
 * Returns whether the given [CharSequence] is phone number.
 * */
fun CharSequence.isPhone(): Boolean = phoneRegex.matches(this)

/**
 * Returns whether the given [CharSequence] is email address.
 * */
fun CharSequence.isEmail(): Boolean = emailRegex.matches(this)

/**
 * Returns whether the given [CharSequence] contains only digits.
 * */
fun CharSequence.isNumeric(): Boolean = TextUtils.isDigitsOnly(this)

/**
 * Returns the MD5 digest as a string containing only hexadecimal digits.
 * */
fun CharSequence.md5() = hexDigest("MD5")

/**
 * Returns the SHA-1 digest as a string containing only hexadecimal digits.
 * */
fun CharSequence.sha1() = hexDigest("SHA-1")

/**
 * Returns the SHA-256 digest as a string containing only hexadecimal digits.
 * */
fun CharSequence.sha256() = hexDigest("SHA-256")

/**
 * Returns the hexadecimal digest string that implements the specified algorithm.
 *
 * @param algorithm the name of the algorithm requested.
 * See the MessageDigest section in the <a href=
 * "https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#MessageDigest">
 * Java Cryptography Architecture Standard Algorithm Name Documentation</a>
 * for information about standard algorithm names.
 *
 * */
fun CharSequence.hexDigest(algorithm: String) = _encrypt(this, algorithm)


private fun _encrypt(sequence: CharSequence, type: String): String {
    val bytes = MessageDigest.getInstance(type).digest(sequence.toString().toByteArray())
    var des = ""
    var tmp: String
    for (i in bytes.indices) {
        tmp = Integer.toHexString(bytes[i].toInt() and 0xFF)
        if (tmp.length == 1) des += "0"
        des += tmp
    }
    return des
}
