# String Extensions

![](https://img.shields.io/badge/latest-v1.0.8-brightgreen)


* CharSequence.isPhone()
* CharSequence.isEmail()
* CharSequence.isNumeric()
* CharSequence.md5()
* CharSequence.sha1()
* CharSequence.sha256()
* CharSequence.hexDigest(algorithm)

Detailed: [StringExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-string/src/main/java/com/hikkidev/ext/string/StringExt.kt)