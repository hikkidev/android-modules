package com.hikkidev.base.error

data class BaseErrorClosure(val error: Throwable) {
    override fun toString(): String {
        return when (error) {
            is BaseError -> "BaseErrorClosure[ ${error.extTraceCode} ]"
            else -> error.toString()
        }
    }
}