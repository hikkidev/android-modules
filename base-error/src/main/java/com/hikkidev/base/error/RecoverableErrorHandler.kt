package com.hikkidev.base.error

import android.util.Log

enum class ErrorRecoveryResult {
    DidHandle, //Ошибка была обработана -> следующий вызов будет потенциально успешным
    CanNotHandle //Этот обработчик не предназначен для устранения ошибок такого рода
}

interface RecoverableErrorHandler {
    suspend fun recover(
        error: BaseError,
        currentRetryAttempt: Int,
        success: suspend (ErrorRecoveryResult) -> Unit,
        failure: (suspend (BaseErrorClosure) -> Unit) = {
            Log.w(javaClass.simpleName, "Произошла ошибка в RecoverableErrorHandler: $it")
        }
    )
}