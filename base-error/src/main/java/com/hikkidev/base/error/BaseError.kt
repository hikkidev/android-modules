package com.hikkidev.base.error

import java.util.*


interface BaseErrorCode {
    val rawCode: Int
}

abstract class BaseError(val code: BaseErrorCode, cause: Throwable?, msg: String?) :
    Throwable(msg, cause) {

    abstract val domainShortName: String

    val rawCode = code.rawCode

    val traceCode: String
        get() = when (val error = cause) {
            is BaseError -> "${domainShortName.toUpperCase(Locale.getDefault())}$rawCode-" + error.traceCode
            else -> "${domainShortName.toUpperCase(Locale.getDefault())}$rawCode"
        }

    val extTraceCode: String
        get() {
            val self = "${javaClass.simpleName}.${code::class.java.simpleName}"
            return when (val error = cause) {
                null -> javaClass.simpleName
                is BaseError -> "$self-" + error.extTraceCode
                else -> "$self-" + error::class.java.simpleName + "." + error.stackTrace.first().methodName
            }
        }

    val rootСauseMessage: String?
        get() = when (val error = cause) {
            null -> localizedMessage
            is BaseError -> error.rootСauseMessage
            else -> error.localizedMessage
        }

    val rootСause: Throwable?
        get() = when (val error = cause) {
            null -> this
            is BaseError -> error.rootСause
            else -> error
        }
}

/**
 * The method spoofes the last element of the stack-trace for normalized display in metrics.
 *
 *                      !!!ATTENTION!!! Irreversible operation.
 * After using this method, @filed:traceCode and @field:extTraceCode will return incorrect values!
 * */
fun <T : BaseError> T.spoofStackTraceForTracking(): T {
    val traceCode = traceCode

    var lastCause: Throwable = cause!!
    while (lastCause.cause != null) {
        lastCause = lastCause.cause!!
    }

    val stackTrace = lastCause.stackTrace
    val newStackTrace = arrayOfNulls<StackTraceElement>(stackTrace.size + 1)
    System.arraycopy(stackTrace, 0, newStackTrace, 1, stackTrace.size)

    newStackTrace[0] = StackTraceElement("", "", traceCode, 0)
    lastCause.stackTrace = newStackTrace
    return this
}

