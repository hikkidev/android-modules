# Dimensions Extensions

![](https://img.shields.io/badge/latest-v1.0.8-brightgreen)


* Number.dp2px
* Number.px2dp
* Number.px2sp
* Number.sp2px
* and transformations

Detailed: [DimentionExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-dimensions/src/main/java/com/hikkidev/ext/dimensions/DimentionExt.kt)