package com.hikkidev.ext.dimensions

import android.content.res.Resources
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.COMPLEX_UNIT_SP

private val metrics = Resources.getSystem().displayMetrics

typealias Pixel = Int

val Float.dp: Float      // [xxhdpi](360 -> 1080)
    get() = TypedValue.applyDimension(COMPLEX_UNIT_DIP, this, metrics)

val Int.dp: Int           // [xxhdpi](360 -> 1080)
    get() = TypedValue.applyDimension(
        COMPLEX_UNIT_DIP, this.toFloat(),
        metrics
    ).toInt()

val Float.sp: Float      // [xxhdpi](360 -> 1080)
    get() = TypedValue.applyDimension(COMPLEX_UNIT_SP, this, metrics)

val Int.sp: Int          // [xxhdpi](360 -> 1080)
    get() = TypedValue.applyDimension(
        COMPLEX_UNIT_SP, this.toFloat(),
        metrics
    ).toInt()

val Number.px: Pixel   // [xxhdpi](360 -> 360)
    get() = this.toInt()

val Number.px2dp: Float
    get() = this.toFloat() / metrics.density

val Number.dp2px: Pixel
    get() = (this.toFloat() * metrics.density).toInt()

val Number.px2sp: Float
    get() = this.toFloat() / metrics.scaledDensity

val Number.sp2px: Pixel
    get() = (this.toFloat() * metrics.scaledDensity).toInt()