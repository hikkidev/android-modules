package com.hikkidev.sample.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hikkideveloper.sample.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}