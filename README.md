# Android Modules
<div align="center">
    <a href="https://kotlinlang.org"><img src="https://img.shields.io/badge/Kotlin-1.4.32-orange?logo=kotlin&logoColor=blue&style=for-the-badge" alt="kotlin" /></a>
</div>
<div align="center">
	<a href="https://www.android.com"><img src="https://img.shields.io/badge/package-AndroidX-brightgreen.svg?style=flat-square" alt="Platform" /></a>
	<a href="https://android-arsenal.com/api?level=6"><img src="https://img.shields.io/badge/API-6%2B-blueviolet.svg?style=flat-square" alt="API" /></a>
	<a href="http://www.apache.org/licenses/LICENSE-2.0"><img src="https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square" alt="License" /></a>
</div>

## Список библиотек
- [ext-bundle](./ext-bundle), модуль содержащий функции расширения для `Bundle`, [BundleExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-bundle/src/main/java/com/hikkidev/ext/bundle/BundleExt.kt) 
- [ext-dimensions](./ext-dimensions), модуль содержащий функции расширения для работы с `Android Dimens`, [DimentionExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-dimensions/src/main/java/com/hikkidev/ext/dimensions/DimentionExt.kt) 
- [ext-string](./ext-string), модуль содержащий функции расширения для `String`, [StringExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-string/src/main/java/com/hikkidev/ext/string/StringExt.kt) 
- [ext-view](./ext-view), модуль содержащий функции расширения для `View` и его наследников, [ViewExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-view/src/main/java/com/hikkidev/ext/view/ViewExt.kt) 


