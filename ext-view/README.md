# View Extensions

![](https://img.shields.io/badge/latest-v1.1.0-brightgreen)


* Actions with visibility
* All padding `setPaddingLeft, setPaddingRight ...`
* Show\Hide keyboard
* TextView.underLine, TextView.deleteLine, TextView.bold
* EditText.value, EditText.passwordToggledVisible
* ViewGroup.inflate, ViewGroup.isEmpty(), ViewGroup.forEach

Detailed: [ViewExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-view/src/main/java/com/hikkidev/ext/view/ViewExt.kt)