package com.hikkidev.ext.view

import android.content.Context
import android.graphics.Paint
import android.os.Build
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi


fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

fun View.hideKeyboard(): Boolean = try {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
} catch (ignored: Exception) {
    false
}

@Suppress("UNCHECKED_CAST")
fun <T : View> T.onClick(block: (T) -> Unit) = setOnClickListener { block(it as T) }

@Suppress("UNCHECKED_CAST")
fun <T : View> T.onLongClick(block: (T) -> Boolean) = setOnLongClickListener { block(it as T) }

val View.isInvisible: Boolean get() = visibility == View.INVISIBLE
val View.isVisible: Boolean get() = visibility == View.VISIBLE
val View.isGone: Boolean get() = visibility == View.GONE

fun View.makeInvisible() {
    if (visibility != View.INVISIBLE) {
        visibility = View.INVISIBLE
    }
}

fun View.makeVisible() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.makeGone() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

inline fun View.makeInvisibleIf(block: () -> Boolean) {
    if (visibility != View.INVISIBLE && block()) {
        visibility = View.INVISIBLE
    }
}

inline fun View.makeVisibleIf(block: () -> Boolean) {
    if (visibility != View.VISIBLE && block()) {
        visibility = View.VISIBLE
    }
}

inline fun View.makeGoneIf(block: () -> Boolean) {
    if (visibility != View.GONE && block()) {
        visibility = View.GONE
    }
}

fun View.setPaddingLeft(value: Int) = setPadding(value, paddingTop, paddingRight, paddingBottom)

fun View.setPaddingRight(value: Int) = setPadding(paddingLeft, paddingTop, value, paddingBottom)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingTop(value: Int) =
    setPaddingRelative(paddingStart, value, paddingEnd, paddingBottom)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingBottom(value: Int) =
    setPaddingRelative(paddingStart, paddingTop, paddingEnd, value)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingStart(value: Int) =
    setPaddingRelative(value, paddingTop, paddingEnd, paddingBottom)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingEnd(value: Int) =
    setPaddingRelative(paddingStart, paddingTop, value, paddingBottom)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingHorizontal(value: Int) =
    setPaddingRelative(value, paddingTop, value, paddingBottom)

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun View.setPaddingVertical(value: Int) = setPaddingRelative(paddingStart, value, paddingEnd, value)

fun TextView.underLine() {
    paint.flags = paint.flags or Paint.UNDERLINE_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.deleteLine() {
    paint.flags = paint.flags or Paint.STRIKE_THRU_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.bold(isBold: Boolean = true) {
    paint.isFakeBoldText = isBold
    paint.isAntiAlias = true
}

var EditText.value: String
    get() = text.toString()
    set(value) = setText(value)

fun EditText.passwordToggledVisible() {
    val selection = selectionStart
    transformationMethod =
        if (transformationMethod == null) PasswordTransformationMethod() else null
    setSelection(selection)
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ViewGroup.isEmpty() = childCount == 0

fun ViewGroup.isNotEmpty() = childCount != 0

fun ViewGroup.forEach(action: (View) -> Unit) {
    for (i in 0..childCount) {
        action(getChildAt(i))
    }
}

fun ViewGroup.forEachIndexed(action: (View, Int) -> Unit) {
    for (i in 0..childCount) {
        action(getChildAt(i), i)
    }
}


